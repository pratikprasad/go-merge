# TODOs 

* port over mergo tests for example structs that we have.
	* manually generate code that passes those tests
	* write code to replace the manually generated code
	* write code that generates tests per struct that you use
* properly handle writing a gen file per package
* generate code that merges structs
* try using it in eave source code
* figure out how to use override merge functions (for example for null types)