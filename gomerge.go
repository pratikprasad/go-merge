package main

import (
	"fmt"
	"os"

	"github.com/urfave/cli"
	"go/parser"
	"go/token"
	"bitbucket.org/pratikprasad/go-merge/mergehelpers"
	"go/ast"
	"bitbucket.org/pratikprasad/go-merge/structs"
)

func main() {
	p1 := &structs.Person{Name: "pratik", Age: 26}
	fmt.Println(p1, p1.City)
	p2 := &structs.Person{DistanceToNearestSubway: 1.45, City: structs.City{Name:"New York",  Mayor: p1}}
	fmt.Println(p2, p1.City)
	p1.Merge(p2)
	fmt.Println(p1, p1.City)
	p3 := &structs.Person{City: structs.City{Population: 1000000}}
	p1.Merge(p3)
	fmt.Println(p1, p1.City)
}

/* TODO:
 * Only generate for structs tagged with "@Merge"
 * If the struct has a Merge function, then use that to merge recursively
 * If the struct doesn't have a merge function, then do the naive thing *(new(<StructName>))
 * ??? What to do about circular dependencies in struct definition?
 */

func GenFileName(pkg string) string {
	return "gen_merge_" + pkg + ".go"
}

func maino() {
	app := cli.NewApp()
	app.Name = "go-merge"
	app.Usage = "Generate some merge functions"
	app.Action = func(c *cli.Context) error {
		filename := c.Args().Get(0)
		fset := token.NewFileSet() // positions are relative to fset
		pkg := mergehelpers.PackageStructMergeData{}
		parsedPkgs, err := parser.ParseDir(fset, filename, nil, 0)
		if err != nil {
			fmt.Println(err)
			return nil
		}
		for packageName, parsedPkg := range parsedPkgs {
			pkg.Package = packageName
			for _, f := range parsedPkg.Files {
				for name, object := range f.Scope.Objects {
					s := mergehelpers.StructMergeData{
						StructName: name,
					}
					fields := []mergehelpers.FieldMergeData{}
					switch object.Decl.(type) {
					case *ast.TypeSpec:
						typeSpec := object.Decl.(*ast.TypeSpec)
						fieldsList := typeSpec.Type.(*ast.StructType).Fields.List
						for _, fieldDecl := range fieldsList {
							currentFieldType := fieldDecl.Type.(*ast.Ident).Name
							//fmt.Println(currentFieldType)
							fields = append(fields, mergehelpers.FieldMergeData{
								FieldName: fieldDecl.Names[0].Name,
								ZeroValue: fmt.Sprintf("*(new(%s))", currentFieldType),
							})
						}
						s.Fields = fields
						pkg.Structs = append(pkg.Structs, s)
					}
				}
			}
		}
		mergehelpers.PrintMergePackage(os.Stdout, pkg)
		return nil
	}
	app.Run(os.Args)
}
