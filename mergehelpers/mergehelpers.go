package mergehelpers

import (
	"fmt"
	"text/template"
	"io"
)

type FieldMergeData struct {
	FieldName string
	ZeroValue string
}

type StructMergeData struct {
	StructName string
	Fields     []FieldMergeData
}

type PackageStructMergeData struct {
	Package string
	Structs []StructMergeData
}

const structMerge = `
package {{.Package}}
{{range .Structs}}
func (s1 {{.StructName}}) Merge(s2 {{.StructName}}) {{.StructName}} {
	{{range .Fields}}
	if s1.{{.FieldName}} == {{.ZeroValue}} {
		s1.{{.FieldName}} = s2.{{.FieldName}}
	}
	{{end}}
	return s1
}
{{end}}
`

// TODO: Add tests
func PrintMergePackage(writer io.Writer, pkg PackageStructMergeData) {
	structTemplate := template.New("Struct Template")
	structTemplate, err := structTemplate.Parse(structMerge)
	if err != nil {
		fmt.Println(err)
	}

	err = structTemplate.Execute(writer, pkg)
	if err != nil {
		fmt.Println(err)
	}
}

/* Example usage of PrintMergePackage
pkg := PackageStructMergeData{
	Package: "structs",
	Structs:[]StructMergeData{
	{
		StructName: "Person",
		Fields: []FieldMergeData{
			{
				FieldName: "Age",
				ZeroValue: "0",
			},
		},
	},
},
}
PrintMergePackage(os.Stdout, pkg)

will generate the following:

---------------------------------------------
package structs

func (s1 Person) Merge(s2 Person) Person {
  if s1.Age == 0 {
  	s1.Age = s2.Age
  }
  return s1
}
---------------------------------------------
 */

