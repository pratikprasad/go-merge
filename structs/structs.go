package structs

import "fmt"

//go:generate go-merge $GOFILE

// @Merge
type Person struct {
	Name string
	Age int64
	City City
	DistanceToNearestSubway float64
}

func (s1 *Person) Merge(s2 *Person) {
	fmt.Println(s1, s2, "Person")
	if s2.City != nil {
		if s1.City == nil {
			s1.City = s2.City
		} else {
			s1.City.Merge(s2.City)
		}
	}
	//if s1.City.m
	if s1.Name == "" {
		s1.Name = s2.Name
	}
	if s1.Age == 0 {
		s1.Age = s2.Age
	}
	if s1.DistanceToNearestSubway == 0 {
		s1.DistanceToNearestSubway = s2.DistanceToNearestSubway
	}
}
