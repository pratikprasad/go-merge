package structs

import "fmt"

type City struct {
	Name string
	Population int64
	Mayor *Person
}

func (s1 *City) Merge(s2 *City) {
	fmt.Println(s1, s2, "City")
	if s2.Mayor != nil {
		if s1.Mayor == nil {
			s1.Mayor = s2.Mayor
		} else {
			s1.Mayor.Merge(s2.Mayor)
		}
	}
	if s1.Name == "" {
		s1.Name = s2.Name
	}
	if s1.Population == 0 {
		s1.Population = s2.Population
	}
}
