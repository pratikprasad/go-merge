# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
 
One common pattern is to "merge" two objects - let's name them _first_ and _second_. 
We'd like the first to have all the properties of the second unless already defined. 

Using reflection isn't canonical so a more appropriate solution is to generate a merge function.

### How do I get set up? ###

go get .../go-merge 

Add the following to an files with structs that need merging.
// go generate gomerge 

If you had a struct of the following type defined:

```
package person

type Person struct {
	Name string
	Age int
}
```

go-merge will generate a function in the same package in a new file that merges structs of the type Person.

```
package person

func (layer1 Person) Merge(layer2 Person) {
	if layer1.Name == "" {
	  layer1.Name = layer2.Name
	}
	if layer1.Age == 0 {
	  layer1.Age = layer2.Age
	}
}
```

Anywhere else that _Person_ is now referenced, you can merge two Person structs by:

```
person1 := Person{ Name: "Alice" }

person2 := Person{ Age: 100 }

fmt.Println(person1)

person1.Merge(person2)

fmt.Println(person1)
```

The above will print:
```
Person { Name: "Alice" } 
Person { Name: "Alice", Age: 100 }
```

